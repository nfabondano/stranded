// Fill out your copyright notice in the Description page of Project Settings.


#include "BP_Projectile.h"

// Sets default values
ABP_Projectile::ABP_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABP_Projectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABP_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

